//
//  WeatherRequest.swift
//  APIClient
//
//  Created by ryookano on 2018/01/04.
//  Copyright © 2018年 ryookano. All rights reserved.
//

import UIKit
import APIKit

struct WeatherRequest: APIKit.Request {

    let tottori = 310010
    let tokyo = 130010
    let cityCode: Int

    var baseURL: URL {
        return URL(string: "http://weather.livedoor.com/forecast/webservice/json")!
    }

    var method: HTTPMethod {
        return .get
    }

    var path: String {
        return "/v1"
    }

    var parameters: Any? {
        return ["city": cityCode]
    }

    typealias Response = WeatherNews
    // MARK: - APIKit.Request

    var dataParser: DataParser {
        return JSONDataParser() as! DataParser
    }

    func response(from object: Any, urlResponse: HTTPURLResponse) throws -> WeatherNews {
        let json = try! JSONSerialization.data(withJSONObject: object, options: [])
        do {
            let hoge = try JSONDecoder().decode(WeatherNews.self, from: json)
            return hoge
        } catch {
            print(error)
            return error as! WeatherNews
        }
    }
}
