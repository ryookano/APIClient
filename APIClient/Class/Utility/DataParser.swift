//
//  DataParser.swift
//  APIClient
//
//  Created by ryookano on 2018/01/04.
//  Copyright © 2018年 ryookano. All rights reserved.
//

import UIKit

public protocol DataParser {
    var contentType: String? { get }
    func parse(data: Data) throws -> Any
}
